//*****************************************************************************
//  PIC12F683 Sample Program
//    Project     : 03_TIMER
//    Create Date : 2013/07/08
//*****************************************************************************
//  Description
//    TIMER0
//    Interrupt
//    GP2 output for LED
//*****************************************************************************

#include<htc.h>

__CONFIG(
          CP_OFF            // Program memory code protection is disabled
        & BOREN_OFF         // BOR disabled
        & MCLRE_OFF         // MCLR pin function is digital input, MCLR internally tied to VDD
        & PWRTE_OFF         // PWRT disabled
        & WDTE_OFF          // WDT disabled
        & FOSC_INTOSCIO );  // INTOSCIO oscillator: I/O function on RA4/OSC2/CLKOUT pin, I/O function on RA5/OSC1/CLKIN

#ifndef _XTAL_FREQ
 // Unless already defined assume 4MHz system frequency
 // This definition is required to calibrate __delay_us() and __delay_ms()
 #define _XTAL_FREQ 4000000
#endif

static long count;

//*****************************************************************************
//  main
//*****************************************************************************
void main(void)
{
    CMCON0 = 0b00000111;
    ANSEL  = 0b00000000;

    GP2 = 1;        //GP2 data High
    TRISIO2 = 0;    //GP2 direction output

    count = 0;
    di();
    /* (1/40000000) * 4 * 2 * 250 = 500us */
    T0CS = 0;       //Internal instruction cycle clock
    PS0  = 0;       //Prescaler Rate 0b000 = 1:2
    PS1  = 0;
    PS2  = 0;
    PSA  = 0;       //Prescaler is assigned to the Timer0 module
    TMR0 = 256 - 250;
    T0IF = 0;       //Clear TIMER0 overflow
    T0IE = 1;       //Enables the Timer0 inerrupt
    ei();

    while(1) ;
}

//*****************************************************************************
//  Interrupt Handler
//*****************************************************************************
static void interrupt isr(void)
{
    T0IF = 0;           //Clear TIMER0 overflow
    TMR0 = 256 - 250;
    if(count<1000){
        count++;
    }else{
        GP2 ^= 1;
        count = 0;
    }
}
