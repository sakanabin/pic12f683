//******************************************************************************
//  PIC12F683 Sample Program
//    Project     : 02_Blink_LED
//    Create Date : 2013/07/07
//*****************************************************************************
//  Description
//    GP2 output for LED
//*****************************************************************************

#include<htc.h>

__CONFIG(
          CP_OFF            // Program memory code protection is disabled
        & BOREN_OFF         // BOR disabled
        & MCLRE_OFF         // MCLR pin function is digital input, MCLR internally tied to VDD
        & PWRTE_OFF         // PWRT disabled
        & WDTE_OFF          // WDT disabled
        & FOSC_INTOSCIO );  // INTOSCIO oscillator: I/O function on RA4/OSC2/CLKOUT pin, I/O function on RA5/OSC1/CLKIN

#ifndef _XTAL_FREQ
 // Unless already defined assume 4MHz system frequency
 // This definition is required to calibrate __delay_us() and __delay_ms()
 #define _XTAL_FREQ 4000000
#endif

//Need for Lite mode
#define __delay_us(x) _delay((unsigned long)((x)*(_XTAL_FREQ/4000000.0)))
#define __delay_ms(x) _delay((unsigned long)((x)*(_XTAL_FREQ/4000.0)))

void main(void)
{
    CMCON0 = 0b00000111;
    ANSEL  = 0b00000000;

    GP2 = 1;        //GP2 data High
    TRISIO2 = 0;    //GP2 direction output

    while(1) {
        GP2 ^= 1;
        __delay_ms(500);
    }
}
