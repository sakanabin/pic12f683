//******************************************************************************
//  PIC12F683 Sample Program
//    Project     : 01_Hello_LED
//    Create Date : 2013/07/07
//*****************************************************************************
//  Description
//    GP2 output for LED
//*****************************************************************************

#include<htc.h>

__CONFIG(
          CP_OFF            // Program memory code protection is disabled
        & BOREN_OFF         // BOR disableed
        & MCLRE_OFF         // MCLR pin function is digital input, MCLR internally tied to VDD
        & PWRTE_OFF         // PWRT disabled
        & WDTE_OFF          // WDT disabled
        & FOSC_INTOSCIO );  // INTOSCIO oscillator: I/O function on RA4/OSC2/CLKOUT pin, I/O function on RA5/OSC1/CLKIN

void main(void)
{
    CMCON0 = 0b00000111;
    ANSEL  = 0b00000000;
 
    GP2 = 1;        //GP2 data High
    TRISIO2 = 0;    //GP2 direction output

    while(1) ;
}
